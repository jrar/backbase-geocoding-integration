﻿## GMaps Geocoding API Integration built with Apache Camel & Spring Boot Microservice

As requested for this assignment, this microservice was built based on:
- Spring Boot Microservice working as a client of Netflix's Eureka service discovery
- Apache Camel integration with Google Geocoding API, consuming their services in XML format
- Maven as building and application management tool
- API Rest endpoint with Camel routes, producing JSON outcome data

## How to build and deploy

Since this application was made as part of a microservices architecture, it needs to be deployed with a running service discovery server. For this example a simple application implementing Eureka was built, so **it needs to be built and run prior to this microservice**.
You can find the GIT repository of this application here:

    https://gitlab.com/jrar/backbase-service-discovery
Since both applications are built with Maven, the process is so simple:

 1.  `mvn install`
  2. `mvn spring-boot:run`

## How to test it

Once the application is up and running it can be tested from any client that allows to send header data inside request.
The endpoint for this service is `http://localhost:8080/lookup/address` and HTTP method accepted is GET.
**Input address needs to be set as a header param with key: 'input'**
An example of a client to test it is Postman:
![Postman test](https://image.ibb.co/k722wV/Sin-t-tulo.png)


**Update**: Added unit tests for the processor class

## Personal thoughts about the assignment

Since I have never worked with Apache Camel or any other Enterprise Integration Framework, this assignment was harder than how it looks currently after finished but I'm happy about the result, despite of the many improvements and unpolished things that the service has (like exception handling, HTTP codes...).
Waiting for feedback, thanks!
Juan
