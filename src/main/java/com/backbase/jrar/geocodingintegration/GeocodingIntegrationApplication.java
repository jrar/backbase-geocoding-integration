package com.backbase.jrar.geocodingintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan
public class GeocodingIntegrationApplication {

  public static void main(String[] args) {
    SpringApplication.run(GeocodingIntegrationApplication.class, args);

  }

}
