package com.backbase.jrar.geocodingintegration.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * POJO used as JSON response from our endpoint
 * 
 * @author Juan Ramón
 *
 */
@JsonPropertyOrder({"formatted_address", "latitude", "longitude"})
@JsonInclude(Include.NON_NULL)
public class GeolocationWrapper {

  @JsonProperty("formatted_address")
  private String formattedAddress;
  private String latitude;
  private String longitude;
  private String error;

  public GeolocationWrapper(String formattedAddress, String latitude, String longitude) {
    this.formattedAddress = formattedAddress;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public GeolocationWrapper(String error) {
    this.error = error;
  }

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setFormattedAddress(String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

}
