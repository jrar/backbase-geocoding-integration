package com.backbase.jrar.geocodingintegration.pojo.external;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GeocodeResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Geolocation {

  @XmlElement
  private String status;
  @XmlElement
  private ResultWrapper result;

  public Geolocation() {}

  public Geolocation(String status, ResultWrapper result) {
    this.status = status;
    this.result = result;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public ResultWrapper getResult() {
    return result;
  }

  public void setResult(ResultWrapper result) {
    this.result = result;
  }

}
