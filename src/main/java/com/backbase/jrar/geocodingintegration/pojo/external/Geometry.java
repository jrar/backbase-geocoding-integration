package com.backbase.jrar.geocodingintegration.pojo.external;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Geometry {

  @XmlElement
  private Location location;

  public Geometry() {}

  public Geometry(Location location) {
    this.location = location;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

}
