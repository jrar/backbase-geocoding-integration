package com.backbase.jrar.geocodingintegration.pojo.external;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResultWrapper {

  @XmlElement(name = "formatted_address")
  private String formattedAddress;
  @XmlElement
  private Geometry geometry;

  public ResultWrapper() {}

  public ResultWrapper(String formattedAddress, Geometry geometry) {
    this.formattedAddress = formattedAddress;
    this.geometry = geometry;
  }

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setFormattedAddress(String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public Geometry getGeometry() {
    return geometry;
  }

  public void setGeometry(Geometry geometry) {
    this.geometry = geometry;
  }

}
