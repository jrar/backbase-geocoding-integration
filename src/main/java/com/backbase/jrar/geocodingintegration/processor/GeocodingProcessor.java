package com.backbase.jrar.geocodingintegration.processor;

import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.camel.ConsumerTemplate;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.backbase.jrar.geocodingintegration.pojo.GeolocationWrapper;
import com.backbase.jrar.geocodingintegration.pojo.external.Geolocation;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Business class to manage the external API queries and compose the outgoing json data
 * 
 * @author Juan Ramón
 *
 */
@Component("geocodingProcessor")
public class GeocodingProcessor {

  private final String ADDRESS_PARAM = "address=";
  private final String API_KEY_PARAM = "key=";

  // Use Camel's ConsumerTemplate integration for an easier retrieval of the external API data
  @Autowired
  private ConsumerTemplate consumerTemplate;

  public GeolocationWrapper getGeolocation(String inputAddress) throws JsonProcessingException {

    // If input address is blank or null, return json with error
    if (StringUtils.isBlank(inputAddress)) {
      return new GeolocationWrapper("Input address can't be empty");
    }

    // Query the external API and store the results as an InputStream
    InputStream rawGeolocation = queryGeocodeApi(inputAddress);

    Geolocation parsedGeolocation = new Geolocation();

    try {
      parsedGeolocation = parseResponseToObject(rawGeolocation);
    } catch (JAXBException e) {
      e.printStackTrace();
      return new GeolocationWrapper("There was an error retrieving the data");
    }

    // If retrieved data from API null or status is not OK, return json with error
    if (parsedGeolocation == null || parsedGeolocation.getStatus() == null
        || !parsedGeolocation.getStatus().equalsIgnoreCase("ok")) {
      return new GeolocationWrapper("We couldn't find any results for the given address");
    }

    // If everything went fine compose the outgoing json POJO and return it
    GeolocationWrapper outgoingGeolocation =
        new GeolocationWrapper(parsedGeolocation.getResult().getFormattedAddress(),
            parsedGeolocation.getResult().getGeometry().getLocation().getLatitude(),
            parsedGeolocation.getResult().getGeometry().getLocation().getLongitude());

    return outgoingGeolocation;
  }

  /**
   * Query Google API and retrieve geolocation for the passed address in XML format
   * 
   * @param inputAddress
   * @return rawGeolocation
   */
  private InputStream queryGeocodeApi(String inputAddress) {
    InputStream rawGeolocation =
        consumerTemplate.receiveBody("{{geocode.api.endpoint}}" + ADDRESS_PARAM + inputAddress + "&"
            + API_KEY_PARAM + "{{geocode.api.key}}", InputStream.class);
    return rawGeolocation;
  }

  /**
   * Parse the incoming XML into our POJO object
   * 
   * @param rawGeolocation
   * @return geolocation POJO
   * @throws JAXBException
   */
  private Geolocation parseResponseToObject(InputStream rawGeolocation) throws JAXBException {
    Geolocation parsedGeolocation;
    JAXBContext jaxbContext = JAXBContext.newInstance(Geolocation.class);
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    parsedGeolocation = (Geolocation) unmarshaller.unmarshal(rawGeolocation);
    return parsedGeolocation;
  }

}
