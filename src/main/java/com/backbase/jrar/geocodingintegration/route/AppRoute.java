package com.backbase.jrar.geocodingintegration.route;

import static org.apache.camel.model.rest.RestParamType.body;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;
import com.backbase.jrar.geocodingintegration.pojo.GeolocationWrapper;

/**
 * Route management class containing the producer API configuration and definition.
 * 
 * @author Juan Ramón
 *
 */
@Component
public class AppRoute extends RouteBuilder {

  @Override
  public void configure() throws Exception {

    // Set route configuration to work with servlet component from Camel and json for API binding
    restConfiguration().component("servlet").bindingMode(RestBindingMode.json)
        .dataFormatProperty("prettyPrint", "true").enableCORS(true);

    // Set API path and consuming/ producing data format
    rest("/lookup").produces("application/json")

        // Set an endpoint on this path, declare the input param and the route destination
        .get("/address").outType(GeolocationWrapper.class).param().name("input").type(body)
        .dataType("string").endParam()
        .to("bean:geocodingProcessor?method=getGeolocation(${header.input})");

  }

}
