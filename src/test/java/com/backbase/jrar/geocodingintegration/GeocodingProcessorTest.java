package com.backbase.jrar.geocodingintegration;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.apache.camel.ConsumerTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.backbase.jrar.geocodingintegration.pojo.GeolocationWrapper;
import com.backbase.jrar.geocodingintegration.processor.GeocodingProcessor;
import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeocodingProcessorTest {

  @MockBean
  private ConsumerTemplate consumerTemplate;

  @Autowired
  private GeocodingProcessor tested;

  /**
   * Test for expected flow when address exists
   * 
   * @throws JsonProcessingException
   * @throws FileNotFoundException
   */
  @Test
  public void getGeolocationOK() throws JsonProcessingException, FileNotFoundException {

    File file = new File("src/test/resources/response-ok.xml");
    InputStream responseOk = new FileInputStream(file);

    Mockito.when(consumerTemplate.receiveBody(Mockito.anyString(), Mockito.anyObject()))
        .thenReturn(responseOk);

    GeolocationWrapper wrapper = tested.getGeolocation("existing address");
    assertTrue(wrapper.getFormattedAddress()
        .equals("1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA"));
    assertNull(wrapper.getError());

  }

  /**
   * Test for expected flow when address does NOT exists
   * 
   * @throws FileNotFoundException
   * 
   * @throws JsonProcessingException
   */
  @Test
  public void getGeolocationNoResults() throws FileNotFoundException, JsonProcessingException {

    File file = new File("src/test/resources/response-noresults.xml");
    InputStream responseNoResults = new FileInputStream(file);

    Mockito.when(consumerTemplate.receiveBody(Mockito.anyString(), Mockito.anyObject()))
        .thenReturn(responseNoResults);

    GeolocationWrapper wrapper = tested.getGeolocation("non-existing address");
    assertTrue(wrapper.getError().equals("We couldn't find any results for the given address"));
    assertNull(wrapper.getFormattedAddress());

  }

  /**
   * Test for expected flow when input address is blank or empty
   * 
   * @throws FileNotFoundException
   * 
   * @throws JsonProcessingException
   */
  @Test
  public void getGeolocationEmptyInput() throws FileNotFoundException, JsonProcessingException {

    GeolocationWrapper wrapper = tested.getGeolocation("");
    assertTrue(wrapper.getError().equals("Input address can't be empty"));
    assertNull(wrapper.getFormattedAddress());

  }

  /**
   * Test for expected flow when external API response is not as expected
   * 
   * @throws FileNotFoundException
   * 
   * @throws JsonProcessingException
   */
  @Test
  public void getGeolocationParseError() throws FileNotFoundException, JsonProcessingException {

    File file = new File("src/test/resources/response-corrupted.xml");
    InputStream responseCorrupted = new FileInputStream(file);

    Mockito.when(consumerTemplate.receiveBody(Mockito.anyString(), Mockito.anyObject()))
        .thenReturn(responseCorrupted);

    GeolocationWrapper wrapper = tested.getGeolocation("test");
    assertTrue(wrapper.getError().equals("There was an error retrieving the data"));
    assertNull(wrapper.getFormattedAddress());

  }

}
